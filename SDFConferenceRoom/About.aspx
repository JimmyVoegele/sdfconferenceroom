﻿<%@ Page Title="About" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="SDFConferenceRoom.About" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h3>You do not have a valid valid user profile for this application.  Please contact <a href=mailto:BHolman@stdavidsfoundation.org?subject=Question%20about%20SDFConfernceRoom&Body=I%20can%20not%20access%20SDFConfernceRoom.>Blake Holman</a> for more information.</h3>
</asp:Content>

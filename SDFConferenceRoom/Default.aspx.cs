﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;

namespace SDFConferenceRoom
{
    public partial class _Default : Page
    {
        static string m_strEarliestDate = "";
        static string m_strLatestDate = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                LoadCalendarDates();

                string strLoggedInUserID = GetLoggedInUser();

                if (string.IsNullOrEmpty(strLoggedInUserID))
                {
                    Response.Redirect("~\\About.aspx");
                }

                lblMain.Text = "SDF Conference Room Events Calendar " + m_strEarliestDate + " - " + m_strLatestDate;
            }
        }

        private string GetLoggedInUser()
        {
            string strLoggedInUserID = HttpContext.Current.User.Identity.Name;

            if (!strLoggedInUserID.ToLower().Contains("stdavidsfoundation"))
            {
                strLoggedInUserID = "";
            }

            return strLoggedInUserID;
        }

        private void LoadCalendarDates()
        {
            string strSelectSQL = "Select Top 1 StartTime from [dbo].[WeeklyCalendar] order by starttime";

            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand(strSelectSQL, conn);
            SqlDataReader reader;
            try
            {
                conn.Open();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    m_strEarliestDate = reader["starttime"].ToString();

                    DateTime dt = Convert.ToDateTime(m_strEarliestDate);
                    m_strEarliestDate = dt.ToShortDateString();
                }
                reader.Close();
            }
            catch (Exception err)
            {
               Console.Write(err);
            }
            finally
            {
                conn.Close();
            }

            strSelectSQL = "Select Top 1 StartTime from [dbo].[WeeklyCalendar] order by starttime desc";
            cmd = new SqlCommand(strSelectSQL, conn);

            try
            {
                conn.Open();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    m_strLatestDate = reader["starttime"].ToString();
                    DateTime dt = Convert.ToDateTime(m_strLatestDate);

                    if (DateTime.Now.AddDays(6) > dt)
                        m_strLatestDate = DateTime.Now.AddDays(6).ToShortDateString(); 
                    else
                        m_strLatestDate = dt.ToShortDateString();

                }
                reader.Close();
            }
            catch (Exception err)
            {
                Console.Write(err);
            }
            finally
            {
                conn.Close();
            }
        }
    }
}
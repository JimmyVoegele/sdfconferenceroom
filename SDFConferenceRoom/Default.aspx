﻿<%@ Page Title="SDF Conference Room Schedule" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SDFConferenceRoom._Default" %>

<%--<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>--%>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <%--<h2 style="color:#0091ba">Conference Room Schedule</h2>--%>

    <asp:Label runat="server" ID="lblMain" Text="" Font-Bold="true" Font-Size="X-Large"  ForeColor="#0091ba"></asp:Label>

    <hr />

<%--    <asp:Label runat="server" ID="RadLabel1" Hidden="True" Text="Show:" Font-Bold="true" ForeColor="#0091ba"></asp:Label>

    <asp:Button ID="btnNext7Days" runat="server" Hidden="True" Text="Next 7 Days" OnClick="btnNext7Days_Click" Font-Bold="true" Skin="Office2007" ></asp:Button>
    <asp:Button ID="btnMonday" runat="server" Hidden="True" OnClick="btnMonday_Click" Skin="Office2007" ></asp:Button>
    <asp:Button ID="btnTuesday" runat="server" Hidden="True" OnClick="btnTuesday_Click" Skin="Office2007" ></asp:Button>
    <asp:Button ID="btnWednesday" runat="server" Hidden="True" OnClick="btnWednesday_Click" Skin="Office2007" ></asp:Button>
    <asp:Button ID="btnThursday" runat="server" Hidden="True" OnClick="btnThursday_Click" Skin="Office2007" ></asp:Button>
    <asp:Button ID="btnFriday" runat="server" Hidden="True" OnClick="btnFriday_Click" Skin="Office2007" ></asp:Button>
    <asp:Button ID="btnSaturday" runat="server" Hidden="True" OnClick="btnSaturday_Click" Skin="Office2007" ></asp:Button>
    <asp:Button ID="btnSunday" runat="server" Hidden="True" OnClick="btnSunday_Click" Skin="Office2007" ></asp:Button>
    <asp:Button ID="btnLast7" runat="server" Hidden="True" Text="Last 7 Days" OnClick="btnLast7_Click" Skin="Office2007" ></asp:Button>
    <hr />--%>

    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" Font-Size="Small" DataSourceID="SqlDataSource1">
        <Columns>
            <asp:BoundField DataField="Location" HeaderText="Location" SortExpression="Location" >
                <ItemStyle Font-Size="Small" HorizontalAlign="Left" Width="250px" />
            </asp:BoundField>
            <asp:BoundField DataField="Organizer" HeaderText="Organizer" SortExpression="Organizer" >
                <ItemStyle Font-Size="Small" HorizontalAlign="Left" Width="250px" />
            </asp:BoundField>
            <asp:BoundField DataField="Subject" HeaderText="Subject" SortExpression="Subject" >
                <ItemStyle Font-Size="Small" HorizontalAlign="Left" Width="500px" />
            </asp:BoundField>
            <asp:BoundField DataField="StartTime" HeaderText="StartTime" SortExpression="StartTime" >
                <ItemStyle Font-Size="Small" HorizontalAlign="Left" Width="250px" />
            </asp:BoundField>
            <asp:BoundField DataField="EndTime" HeaderText="EndTime" SortExpression="EndTime" >
                <ItemStyle Font-Size="Small" HorizontalAlign="Left" Width="250px" />
            </asp:BoundField>
        </Columns>
</asp:GridView>
<asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:SDFConfRoomsConnectionString %>" SelectCommand="SELECT * FROM [WeeklyCalendar] ORDER BY [StartTime], [Location]"></asp:SqlDataSource>
</asp:Content>
